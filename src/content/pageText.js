import { Firebase } from "../services/firebase";

/**
 * Get Page Content from Firebase
 * 
 * Promises which retrive data from firebase to load the page.
 */
export const pT = Firebase.getBannerText();
export const sL = Firebase.getSoundCloudFrames();


// Below this are backups for incase Firebase messes up
// ------------------------------------------------------------


/**
 * String Legend
 * 
 * `%s%` - Text inbetween these will be interpreted as `<strong>`
 */
export const pageText = {
  banner: {
    title: 'The Ripple Effect',
    subtitle: 'Do you feel that? The cause and effect, the binding force that moves you just beyond the world you thought was possible. Welcome to the land of dreamers, your highness %awaits.%'
  }
}

/**
 * Sound Cloud Items
 * 
 * Links to soundcloud links for Leslie King
 */
export const soundCloudItems = [
  "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/579463653&color=%236c6c6c&auto_play=false&hide_related=true&show_comments=false&show_user=false&show_reposts=false&show_teaser=false&visual=true",

  "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/579021120&color=%23585050&auto_play=false&hide_related=true&show_comments=false&show_user=false&show_reposts=false&show_teaser=false&visual=true",

  "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/585708330&color=%23544c47&auto_play=false&hide_related=true&show_comments=false&show_user=false&show_reposts=false&show_teaser=false&visual=true",

  "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/336658055&color=%23544c47&auto_play=false&hide_related=true&show_comments=false&show_user=false&show_reposts=false&show_teaser=false&visual=true"
];

/**
 * News Items
 * 
 * Latest news on Leslie King
 */
export const newsItems = [
  {
    title: 'Leslie King going Stupid on the beat with Tiéno',
    date: new Date().toDateString()
  },
  {
    title: 'Leslie King ZoneOut Session now avaliable on *YouTube*',
    subtitle: 'ZoneOut Sessions recognize the legend on the rise. Check out his performance and ride the wave',
    date: new Date().toDateString()
  },
  {
    title: 'Preludes to The Ripple Effect out now on *SoundCloud*',
    date: new Date().toDateString()
  }
];