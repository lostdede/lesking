import React, { Component } from 'react';
import './styles.scss';

export default class SectionHeader extends Component {

  render() {
    const { title } = this.props;

    return (
      <div className="sectionHeader">
        <p className="sectionHeader-title">{title}</p>
      </div>
    );
  }
}