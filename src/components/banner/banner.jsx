import React, { Component } from 'react';
import { FaInstagram, FaSoundcloud } from 'react-icons/fa';
import { convert } from '../../utils/markdown';
import { detect } from 'detect-browser';
// Images
import brandLogoSvg from '../../assets/LK-Large-logo.svg';
import brandLogoPng from '../../assets/LK-Large-logo.png';
import './styles.scss';

const browser = detect();
const brandLogo = browser.name === 'safari' ? brandLogoPng : brandLogoSvg;

const socialLinks = [
  {
    name: 'SoundCloud',
    icon: (<FaSoundcloud></FaSoundcloud>),
    link: 'https://soundcloud.com/leslie-idornigie'
  },
  {
    name: 'Instagram',
    icon: (<FaInstagram></FaInstagram>),
    link: 'https://www.instagram.com/rhythmandpoetry_/'
  }
]
export default class Banner extends Component {

  get socialSharingLinks() {
    return socialLinks.map(l => (<button key={l.name} alt={l.name} onClick={() => window.open(l.link, '_blank')} className="banner-social-button">{l.icon}</button>));
  }

  
  render() {
    const { title, subtitle } = this.props;

    return (
      <div className="banner-container">
        <div className="banner-image">
          <img
            className="banner-image-logo"
            alt="Leslie King"
            src={brandLogo} />
        </div>
        <div className="banner-text">
      <div className="banner-title">{title}</div>
      <div className="banner-subtitle">{convert(subtitle)}</div>
        </div>
      <div className="banner-social-links">{this.socialSharingLinks}</div>
      </div>
    )
  }
}