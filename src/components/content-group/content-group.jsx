import React, { Component } from 'react';
import './styles.scss';

export default class ContentGroup extends Component {

  render() {
    let { items } = this.props;

    items = items.map(i => {
      return (
        <div className="content-group-item" key={items.indexOf(i)}>
          {i}
        </div>
      );
    });

    return (
      <div className="content-group-container">
       {items} 
      </div>
    )
  }
}