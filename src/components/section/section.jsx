import React, { Component } from 'react';
import './styles.scss';
import SectionHeader from '../ui/section-header/section-header';

export default class Section extends Component {

  render() {
    const { title, children } = this.props;

    return (
      <div className="section-container">
        <SectionHeader title={title}/>
        <div className="section-content">
          {children}
        </div>
      </div>
    )
  }
}