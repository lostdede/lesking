import React, { Component } from 'react';
import './styles.scss';
import { convert } from '../../utils/markdown';
import { Firebase } from '../../services/firebase';

export default class NewsList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newsItemElements: []
    }

    Firebase.leslieKingNews().then(items => {
      const itemList = [];
      let sortedItems = items.sort((a,b) => b.date.seconds - a.date.seconds);

      for (let i of sortedItems) {
        itemList.push(this.createNewsItem(i, items.indexOf(i)));
      }

      this.setState({
        newsItemElements: itemList
      });
    });
  }

  createNewsItem(item, indx) {
    const subtitle = item.subtitle ? (<div className="news-list-item-subtitle">
      {convert(item.subtitle)}
    </div>) : "";

      return (
        <div className="news-list-item" key={indx}>
          <div className="news-list-item-title">
            {convert(item.title)}
          </div>
          {subtitle}
          <div className="news-list-item-date">
            Posted: {new Date(item.date.seconds * 1000).toDateString()}
          </div>
        </div>
      );
  }

  render() {
    const { newsItemElements } = this.state;

    return (
      <div className="news-list-container">
        {newsItemElements}
      </div>
    )
  }

}