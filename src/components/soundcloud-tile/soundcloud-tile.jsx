import React, { Component } from 'react';
import './styles.scss';

export default class SoundCloudTile extends Component {

  render() {
    const { link } = this.props;

    return (
      <iframe 
        title={`Soundcloud iframe for ${link}`}
        className="soundcloud-musicTile"
        scrolling="no" 
        frameBorder="no" 
        allow="autoplay" 
        src={link}></iframe>
    )
  }
}