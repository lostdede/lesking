import React, { Component } from 'react';
import './styles.scss';
import Banner from '../banner/banner';
import Section from '../section/section';
import ContentGroup from '../content-group/content-group';
import SoundCloudTile from '../soundcloud-tile/soundcloud-tile';
import NewsList from '../news-list/news-list';
import { pageText, pT, sL, soundCloudItems} from '../../content/pageText';


export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      banner: pageText.banner,
      soundCloudItems: soundCloudItems.map(s => (<SoundCloudTile link={s} />)),
    }

    pT.then(r => this.setState({banner: r}))
    sL.then(r => this.setState({
      soundCloudItems: r.map(s => (<SoundCloudTile link={s} />))
    }))
  }


  render() {
    const {banner, soundCloudItems } = this.state;
    return (
      <div className="home-container">
        <Banner title={banner.title} subtitle={banner.subtitle}/>
        <div className="sections-container">
          <Section title="Out Now">
            <div className="section-video">
              <iframe title="Leslie King - Zone Out Session" src="https://www.youtube.com/embed/jA-X_56aErM" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            </div>
          </Section>
          <Section title="Latest Releases">
            <ContentGroup items={soundCloudItems} />
          </Section>
          <Section title="News">
            <NewsList/>
          </Section>
        </div>
      </div>
    );
  }
}
