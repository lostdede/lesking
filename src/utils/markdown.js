import ReactHTMLParser from 'react-html-parser';

/**
 * Convert
 * 
 * Converts a markdown string into an html parseable string.
 * @param {String} str - Markdown formatted string 
 */
export function convert(str) {
  const boldMatch = /\*(.*?)\*/g;
  const underlineMatch = /_(.*?)_/g;
  const italicsMatch = /\/(.*?)\//g;
  const strongMatch = /%(.*?)%/g;

  if (!str) {
    return '';
  }

  const boldText = str.match(boldMatch);
  const underlineText = str.match(underlineMatch);
  const italicsText = str.match(italicsMatch);
  const strongText = str.match(strongMatch);

  if (boldText) {
    for (let t of boldText) {
      str = str.replace(t, `<span class="markdown-bold">${t.replace(/\*/g,'')}</span>`);
    }
  }

  if (underlineText) {
    for (let t of underlineText) {
      str = str.replace(t, `<span class="markdown-underline">${t.replace(/_/g,'')}</span>`);
    }
  }

  if (italicsText) {
    for (let t of italicsText) {
      str = str.replace(t, `<span class="markdown-italics">${t.replace(/\//g,'')}</span>`);
    }
  }

  if (strongText) {
    for (let t of strongText) {
      str = str.replace(t, `<span class="markdown-strong">${t.replace(/%/g,'')}</span>`);
    }
  }

  return ReactHTMLParser(str);
}

