import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const config = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
};

app.initializeApp(config);

class Firebase {
  constructor() {
    const fb = app.firestore();
    this.db = fb.collection(process.env.REACT_APP_FB_COLLECTION).doc(process.env.REACT_APP_FB_SUB_DOMAIN);

    this.collections = {
      news: this.db.collection(`news`),
      pageText: this.db.collection(`pageText`),
    }
  }

  async getBannerText() {
    const doc = await this.collections.pageText.doc('banner').get();
    return doc.data();
  }

  async getSoundCloudFrames() {
    const doc = await this.collections.pageText.doc('soundcloud').get();
    return doc.data().songs;
  }

  async leslieKingNews() {
    const newsDocs = await this.collections.news.get();
    return newsDocs.docs.map(d => d.data());
  }
}


export default new Firebase();;

